package com.ticket.tk_auth_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TkAuthServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TkAuthServiceApplication.class, args);
    }

}
